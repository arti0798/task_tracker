import Header from './components/Header';
import Tasks from './components/Tasks'
import { useState } from 'react';

function App() {

  const [tasks, setTasks] = useState([
    {
        id:1,
        text:"Doctor Appointemnt",
        day: "7 Feb 2022",
        remainder: true,
    },
    {
        id:2,
        text:"College Lecture",
        day: "8 Feb 2022 at 9am",
        remainder: true,
    },
    {
        id:3,
        text:"Food Shopping",
        day: "10 Feb 2022 at 11am",
        remainder: false,
    }
])

  return (
    <div className="App">
      <Header title="Task Tracker"/>
      <Tasks tasks={tasks}/>
    </div>
  );
}

export default App;
