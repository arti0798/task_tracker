import React from 'react';
import PropTypes from 'prop-types';



const Tasks = ({tasks}) => {
    

  return (
        <>
            {tasks.map(task => (<h1 key={task.id}> {task.text}</h1>))}
        </>
    );
};

Tasks.propTypes = {};

export default Tasks;
